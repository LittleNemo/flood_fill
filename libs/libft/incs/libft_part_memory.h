/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_memory.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:36:26 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:55:31 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_MEMORY_H
# define LIBFT_PART_MEMORY_H

# include <stdlib.h>
# include "libft_typedefs.h"

/*
**	These functions are part of the memory portion of the libft
*/
int				ft_memcmp(const void *s1, const void *s2, size_t n);
t_bool			ft_memdel(void **ap);
void			ft_bzero(void *s, size_t n);
void			*ft_memalloc(size_t size);
void			*ft_memccpy(void *dst, const void *src, int c, size_t n);
void			*ft_memchr(const void *s, int c, size_t n);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memmove(void *dst, const void *src, size_t len);
void			*ft_memset(void *b, int c, size_t len);

#endif
