/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_math.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:39:03 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/03 14:50:48 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_MATH_H
# define LIBFT_PART_MATH_H

# include <stdlib.h>
# include "libft_defines.h"
# include "libft_typedefs.h"
# include "libft_part_string.h"
# include "libft_part_type.h"

/*
**	These functions are part of the math portion of the libft
*/
char			*ft_itoa(int n);
char			*ft_itoa_base(int n, int base);
double			ft_ceil(double x) __attribute__((always_inline));
double			ft_floor(double x) __attribute__((always_inline));
int				ft_atoi(const char *str);
int				ft_isprime(int n);
int				ft_nextprime(int n);
int				ft_pgcd(int a, int b);
int				ft_pow(int n, int exp);
int				ft_ppcm(int a, int b);
int				ft_sqrt(int nb);
long			ft_atol(const char *str);
long long		ft_abs(long long n);
size_t			ft_numlen(intmax_t n);
size_t			ft_numlen_base(intmax_t n, int base);

#endif
