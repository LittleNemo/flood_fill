/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_type.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:32:24 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/03 14:56:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_TYPE_H
# define LIBFT_PART_TYPE_H

/*
**	These functions are part of the type portion of the libft
*/
int				ft_isalnum(int c) __attribute__((always_inline));
int				ft_isalpha(int c) __attribute__((always_inline));
int				ft_isascii(int c) __attribute__((always_inline));
int				ft_isblank(int c) __attribute__((always_inline));
int				ft_iscntrl(int c) __attribute__((always_inline));
int				ft_isdigit(int c) __attribute__((always_inline));
int				ft_isgraph(int c) __attribute__((always_inline));
int				ft_islower(int c) __attribute__((always_inline));
int				ft_isprint(int c) __attribute__((always_inline));
int				ft_ispunct(int c) __attribute__((always_inline));
int				ft_issdigit(int c) __attribute__((always_inline));
int				ft_isspace(int c) __attribute__((always_inline));
int				ft_isupper(int c) __attribute__((always_inline));
int				ft_isxdigit(int c) __attribute__((always_inline));
int				ft_str_istype(const char *str, int (*f)(int));

#endif
