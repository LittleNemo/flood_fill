/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_typedefs.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 16:01:01 by lbrangie          #+#    #+#             */
/*   Updated: 2019/02/21 19:17:56 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_TYPEDEFS_H
# define LIBFT_TYPEDEFS_H

typedef struct	s_list	{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

# ifndef __STDBOOL_H

typedef enum	e_bool	{
	error = -1,
	false = 0,
	true = 1
}				t_bool;

# endif

#endif
