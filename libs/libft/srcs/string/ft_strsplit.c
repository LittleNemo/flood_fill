/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 16:45:34 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:28:57 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strsplit() function allocates an null-terminated array of
**	null-terminated strings representing the spliting of the string s on every
**	characters c whitin it.
**
** RETURN VALUES
**	- ft_strsplit() returns a pointer to the allocated array, or NULL if an
**	error occured.
*/

char			**ft_strsplit(const char *s, char c)
{
	return (ft_strusplit(s, &c));
}
