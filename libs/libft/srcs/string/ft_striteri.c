/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 17:09:00 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:32:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_striteri() function applies the f function on every characters of
**	the string s by giving to f the address of the character and the index of
**	this character.
**
** RETURN VALUE
**	- ft_striteri() returns true if f has been applied succesfully. Otherwise,
**	false is returned.
*/

t_bool			ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	int				i;

	if (!s || !f)
		return (false);
	i = -1;
	while (s[++i])
		f(i, &s[i]);
	return (true);
}
