/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puttab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 15:38:06 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:19:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_puttab() function attempts to write the content of the table tab on
**	the standard output, a.k.a. sdtout.
*/

void			ft_puttab(char **tab)
{
	int				i;

	if (!tab)
		return ;
	i = -1;
	while (tab[++i])
		ft_putstr_endl(tab[i]);
}
