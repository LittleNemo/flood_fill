/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_endl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 15:54:47 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:18:30 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_putstr_endl() function attempts to write the string s followed by a
**	'\n' character on the standard output, a.k.a. stdout.
**
** RETURN VALUES
**	- ft_putstr_endl() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putstr_endl(const char *s)
{
	return (write(1, s, ft_strlen(s)) + write(1, "\n", 1));
}
