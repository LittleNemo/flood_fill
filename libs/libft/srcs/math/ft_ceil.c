/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ceil.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 14:44:21 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/03 14:46:46 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_math.h"

/*
** DESCRIPTION
**	- The ft_ceil() function computes the smallest integer greater than x.
**
** RETURN VALUES
**	- ft_ceil() returns the value computed.
*/

double			ft_ceil(double x)
{
	return ((int)x + 1);
}
