/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_floor.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 14:43:34 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/03 14:48:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_math.h"

/*
** DESCRIPTION
**	- The ft_floor() function computes the largest integer less than or equal to
**	x.
**
** RETURN VALUES
**	- ft_floor() returns the value computed.
*/

double			ft_floor(double x)
{
	return ((int)x);
}
