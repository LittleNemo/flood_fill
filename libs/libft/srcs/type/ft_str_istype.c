/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_istype.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 20:42:01 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:46:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_type.h"

/*
** DESCRIPTION
**	- The ft_str_istype() function tests each characters of the string str using
**	the f function. ft_str_istype() should be use with:
**		- ft_isalnum()		- ft_isalpha()		- ft_isascii()
**		- ft_isblank()		- ft_iscntrl()		- ft_isdigit()
**		- ft_isgraph()		- ft_islower()		- ft_isprint()
**		- ft_ispunct()		- ft_issdigit()		- ft_isspace()
**		- ft_isupper()		- ft_isxdigit()
**
** RETURN VALUES
**	- ft_str_istype() returns 1 if the test is true or 0 if the test is false.
*/

int				ft_str_istype(const char *str, int (*f)(int))
{
	while (*str)
		if (!f(*str++))
			return (0);
	return (1);
}
