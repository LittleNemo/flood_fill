/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_issdigit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/21 18:14:57 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:49:30 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_type.h"

/*
** DESCRIPTION
**	- The ft_isdigit() function tests if the integer c is a decimal-digit
**	character or a sign character.
**	This includes the following characters (preceded by their decimal values)
**		 43 '+' 	 45 '-' 	 48 '0'		 49 '1'		 50 '2'
**		 51 '3'		 52 '4' 	 53 '5'		 54 '6'		 55 '7'
**		 56 '8'		 57 '9'
**
** RETURN VALUES
**	- ft_isdigit() returns 1 if the test is true or 0 if the test is false.
*/

int				ft_issdigit(int c)
{
	return (ft_isdigit(c) || c == '+' || c == '-');
}
