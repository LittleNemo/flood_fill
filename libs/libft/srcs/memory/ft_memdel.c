/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 16:40:55 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:52:53 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_memory.h"

/*
** DESCRIPTION
**	- The ft_memdel() function frees the allocated memory of the pointer *ap,
**	and sets the pointer to NULL.
**
** RETURN VALUE
**	- ft_memdel() returns true if the line has succesfully be deleted.
**	Otherwise, false is returned.
*/

t_bool			ft_memdel(void **ap)
{
	if (!ap || !*ap)
		return (false);
	free(*ap);
	*ap = NULL;
	return (true);
}
