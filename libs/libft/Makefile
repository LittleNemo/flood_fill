# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/10 15:58:04 by lbrangie          #+#    #+#              #
#    Updated: 2019/03/19 19:41:33 by lbrangie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################################################################
##                                   TARGET                                   ##
################################################################################
NAME				=		libft.a

################################################################################
##                                   SOURCES                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
SRCS_DIR			=		srcs/
SRCS_DIR_IO			=		io/
SRCS_DIR_LIST		=		list/
SRCS_DIR_MATH		=		math/
SRCS_DIR_MEM		=		memory/
SRCS_DIR_STR		=		string/
SRCS_DIR_TYPE		=		type/

# ================================= FILES ================================= #
FILES_IO			=		ft_putchar.c								\
							ft_putchar_endl.c							\
							ft_putchar_endl_fd.c						\
							ft_putchar_fd.c								\
							ft_putfile.c								\
							ft_putfile_fd.c								\
							ft_putstr.c									\
							ft_putstr_endl.c							\
							ft_putstr_endl_fd.c							\
							ft_putstr_fd.c								\
							ft_puttab.c									\
							ft_puttab_fd.c								\
							get_next_line.c

FILES_LIST			=		ft_lstadd.c									\
							ft_lstdel.c									\
							ft_lstdelone.c								\
							ft_lstiter.c								\
							ft_lstmap.c									\
							ft_lstnew.c

FILES_MATH			=		ft_abs.c									\
							ft_atoi.c									\
							ft_atol.c									\
							ft_isprime.c								\
							ft_itoa.c									\
							ft_itoa_base.c								\
							ft_nextprime.c								\
							ft_numlen.c									\
							ft_numlen_base.c							\
							ft_pgcd.c									\
							ft_pow.c									\
							ft_ppcm.c									\
							ft_sqrt.c

FILES_MEM			=		ft_bzero.c									\
							ft_memalloc.c								\
							ft_memccpy.c								\
							ft_memchr.c									\
							ft_memcmp.c									\
							ft_memcpy.c									\
							ft_memdel.c									\
							ft_memmove.c								\
							ft_memset.c

FILES_STR			=		ft_str_tolower.c							\
							ft_str_toupper.c							\
							ft_strcasechr.c								\
							ft_strcasecmp.c								\
							ft_strcasestr.c								\
							ft_strcat.c									\
							ft_strchr.c									\
							ft_strclr.c									\
							ft_strcmp.c									\
							ft_strcpy.c									\
							ft_strcprg.c								\
							ft_strcspn.c								\
							ft_strdel.c									\
							ft_strdup.c									\
							ft_strequ.c									\
							ft_striter.c								\
							ft_striteri.c								\
							ft_strjoin.c								\
							ft_strlcat.c								\
							ft_strlen.c									\
							ft_strmap.c									\
							ft_strmapi.c								\
							ft_strncasecmp.c							\
							ft_strncasestr.c							\
							ft_strncat.c								\
							ft_strncmp.c								\
							ft_strncpy.c								\
							ft_strndup.c								\
							ft_strnequ.c								\
							ft_strnew.c									\
							ft_strnlen.c								\
							ft_strnstr.c								\
							ft_strpbrk.c								\
							ft_strprg.c									\
							ft_strrcasechr.c							\
							ft_strrchr.c								\
							ft_strrev.c									\
							ft_strsplit.c								\
							ft_strspn.c									\
							ft_strstr.c									\
							ft_strsub.c									\
							ft_strtok.c									\
							ft_strtrim.c								\
							ft_strctrim.c								\
							ft_strusplit.c								\
							ft_tabdel.c									\
							ft_tablen.c									\
							ft_tolower.c								\
							ft_toupper.c

FILES_TYPE			=		ft_isalnum.c								\
							ft_isalpha.c								\
							ft_isascii.c								\
							ft_isblank.c								\
							ft_iscntrl.c								\
							ft_isdigit.c								\
							ft_isgraph.c								\
							ft_islower.c								\
							ft_isprint.c								\
							ft_ispunct.c								\
							ft_issdigit.c								\
							ft_isspace.c								\
							ft_isupper.c								\
							ft_isxdigit.c								\
							ft_str_istype.c

# ================================ SOURCES ================================ #
SRCS_IO				=		$(addprefix $(SRCS_DIR_IO), $(FILES_IO))
SRCS_LIST			=		$(addprefix $(SRCS_DIR_LIST), $(FILES_LIST))
SRCS_MATH			=		$(addprefix $(SRCS_DIR_MATH), $(FILES_MATH))
SRCS_MEM			=		$(addprefix $(SRCS_DIR_MEM), $(FILES_MEM))
SRCS_STR			=		$(addprefix $(SRCS_DIR_STR), $(FILES_STR))
SRCS_TYPE			=		$(addprefix $(SRCS_DIR_TYPE), $(FILES_TYPE))

SRCS				=		$(addprefix $(SRCS_DIR), $(SRCS_IO))		\
							$(addprefix $(SRCS_DIR), $(SRCS_LIST))		\
							$(addprefix $(SRCS_DIR), $(SRCS_MATH))		\
							$(addprefix $(SRCS_DIR), $(SRCS_MEM))		\
							$(addprefix $(SRCS_DIR), $(SRCS_STR))		\
							$(addprefix $(SRCS_DIR), $(SRCS_TYPE))


################################################################################
##                                   OBJECTS                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
OBJS_DIR			=		objs/
OBJS_DIR_IO			=		io/
OBJS_DIR_LIST		=		list/
OBJS_DIR_MATH		=		math/
OBJS_DIR_MEM		=		memory/
OBJS_DIR_STR		=		string/
OBJS_DIR_TYPE		=		type/

# ================================ OBJECTS ================================ #
OBJS_IO				=		$(addprefix $(OBJS_DIR_IO), $(FILES_IO:.c=.o))
OBJS_LIST			=		$(addprefix $(OBJS_DIR_LIST), $(FILES_LIST:.c=.o))
OBJS_MATH			=		$(addprefix $(OBJS_DIR_MATH), $(FILES_MATH:.c=.o))
OBJS_MEM			=		$(addprefix $(OBJS_DIR_MEM), $(FILES_MEM:.c=.o))
OBJS_STR			=		$(addprefix $(OBJS_DIR_STR), $(FILES_STR:.c=.o))
OBJS_TYPE			=		$(addprefix $(OBJS_DIR_TYPE), $(FILES_TYPE:.c=.o))

OBJS_IO_F			=		$(addprefix $(OBJS_DIR), $(OBJS_IO))
OBJS_LIST_F			=		$(addprefix $(OBJS_DIR), $(OBJS_LIST))
OBJS_MATH_F			=		$(addprefix $(OBJS_DIR), $(OBJS_MATH))
OBJS_MEM_F			=		$(addprefix $(OBJS_DIR), $(OBJS_MEM))
OBJS_STR_F			=		$(addprefix $(OBJS_DIR), $(OBJS_STR))
OBJS_TYPE_F			=		$(addprefix $(OBJS_DIR), $(OBJS_TYPE))

OBJS				=		$(addprefix $(OBJS_DIR), $(OBJS_IO))		\
							$(addprefix $(OBJS_DIR), $(OBJS_LIST))		\
							$(addprefix $(OBJS_DIR), $(OBJS_MATH))		\
							$(addprefix $(OBJS_DIR), $(OBJS_MEM))		\
							$(addprefix $(OBJS_DIR), $(OBJS_STR))		\
							$(addprefix $(OBJS_DIR), $(OBJS_TYPE))


################################################################################
##                                  COMPILING                                 ##
################################################################################
# ============================== COMPILATOR =============================== #
CC					=		gcc
CFLAGS				+=		-Wall -Wextra -Werror -Ofast
	
# =============================== INCLUDES ================================ #
INC_DIR				=		incs/
INC_FILE_IO			=		libft_part_io.h
INC_FILE_LIST		=		libft_part_list.h
INC_FILE_MATH		=		libft_part_math.h
INC_FILE_MEM		=		libft_part_memory.h
INC_FILE_STR		=		libft_part_string.h
INC_FILE_TYPE		=		libft_part_type.h
INC_FILE_DEFINES	=		libft_defines.h
INC_FILE_TYPEDEFS	=		libft_typedefs.h
INC_COMP			=		-I$(INC_DIR)

INC_LIBFT			=		$(addprefix $(INC_DIR), $(INC_FILE_IO))		\
							$(addprefix $(INC_DIR), $(INC_FILE_LIST))	\
							$(addprefix $(INC_DIR), $(INC_FILE_MATH))	\
							$(addprefix $(INC_DIR), $(INC_FILE_MEM))	\
							$(addprefix $(INC_DIR), $(INC_FILE_STR))	\
							$(addprefix $(INC_DIR), $(INC_FILE_TYPE))

# =============================== LIBRARIES =============================== #
LIB_NAME			=		ft
LIB_PATH			=		./lib$(LIB_NAME)
LIB_COMP			=		-Llib$(LIB_NAME) -l$(LIB_NAME)
LIB_ARCV			=		lib$(LIB_NAME).a
AR					=		ar rc
RAN					=		ranlib

# ================================ SLAVES ================================= #
MAKESLV				=		make -C


################################################################################
##                                  DELETION                                  ##
################################################################################
RM					=		rm -f
RM_DIR				=		rm -rfd


################################################################################
##                                    RULES                                   ##
################################################################################

all					:		$(NAME)

proper				:
	@$(MAKESLV) . all
	@$(MAKESLV) . clean



$(OBJS_DIR)$(OBJS_DIR_TYPE)%.o		:		$(SRCS_DIR)$(SRCS_DIR_TYPE)%.c $(INC_DIR)$(INC_LIBFT_TYPE) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)$(OBJS_DIR_STR)%.o		:		$(SRCS_DIR)$(SRCS_DIR_STR)%.c $(INC_DIR)$(INC_LIBFT_STR) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)$(OBJS_DIR_MEM)%.o		:		$(SRCS_DIR)$(SRCS_DIR_MEM)%.c $(INC_DIR)$(INC_LIBFT_MEM) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)$(OBJS_DIR_MATH)%.o		:		$(SRCS_DIR)$(SRCS_DIR_MATH)%.c $(INC_DIR)$(INC_LIBFT_MATH) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)$(OBJS_DIR_LIST)%.o		:		$(SRCS_DIR)$(SRCS_DIR_LIST)%.c $(INC_DIR)$(INC_LIBFT_LIST) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)

$(OBJS_DIR)$(OBJS_DIR_IO)%.o		:		$(SRCS_DIR)$(SRCS_DIR_IO)%.c $(INC_DIR)$(INC_LIBFT_IO) Makefile
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)



$(OBJS_DIR)			:
	@mkdir -p $@
	@mkdir -p $@/$(OBJS_DIR_IO)
	@mkdir -p $@/$(OBJS_DIR_LIST)
	@mkdir -p $@/$(OBJS_DIR_MATH)
	@mkdir -p $@/$(OBJS_DIR_MEM)
	@mkdir -p $@/$(OBJS_DIR_STR)
	@mkdir -p $@/$(OBJS_DIR_TYPE)

$(NAME)				:		$(OBJS_DIR) $(OBJS_IO_F) $(OBJS_LIST_F) $(OBJS_MATH_F) $(OBJS_MEM_F) $(OBJS_STR_F) $(OBJS_TYPE_F)
	@$(AR) $@ $(OBJS) 
	@$(RAN) $@



clean				:
	@$(RM_DIR) $(OBJS_DIR)

fclean				:		clean
	@$(RM) $(NAME)

re					:
	@$(MAKESLV) . fclean
	@$(MAKESLV) . all



norme 				:		$(INC_DIR) $(SRCS_DIR)
	@norminette $^ | grep -B 1 "Warning\|Error" || true

print-%				:
	@echo $($*)

.PHONY				:		all clean fclean re start norme
