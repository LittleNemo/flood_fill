# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/16 10:26:59 by lbrangie          #+#    #+#              #
#    Updated: 2019/04/25 15:36:07 by lbrangie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################################################################
##                                   TARGET                                   ##
################################################################################
NAME_FLOOD_FILL		=		flood_fill


################################################################################
##                                   SOURCES                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
SRCS_DIR			=		srcs/

# ================================= FILES ================================= #
FILES_FLOOD_FILL	=		flood_fill.c			\
							ff_error.c				\
							ff_file.c				\
							ff_meta.c				\
							ff_solve.c

# ================================ SOURCES ================================ #
SRCS_FLOOD_FILL		=		$(addprefix $(SRCS_DIR), $(FILES_FLOOD_FILL))


################################################################################
##                                   OBJECTS                                  ##
################################################################################
# ============================== DIRECTORIES ============================== #
OBJS_DIR			=		objs/

# ================================ OBJECTS ================================ #
OBJS_FLOOD_FILL		=		$(addprefix $(OBJS_DIR), $(FILES_FLOOD_FILL:.c=.o))


################################################################################
##                                  COMPILING                                 ##
################################################################################
# ============================== COMPILATOR =============================== #
CC					=		gcc
CFLAGS				+=		-Wall -Wextra -Werror -g3

# =============================== INCLUDES ================================ #
INC_DIR				=		incs/
INC_FILE			=		flood_fill.h				\
							flood_fill_defines.h		\
							flood_fill_typedefs.h

INC_COMP			=		-I$(INC_DIR) -Ilibs/libft/incs -Ilibs/ft_printf/includes
INC_FLOOD_FILL		=		$(addprefix $(INC_DIR), $(INC_FILE))

# =============================== LIBRARIES =============================== #
LIB_DIR				=		libs/

LIBPTF_PATH			=		./$(LIB_DIR)ft_printf/
LIBPTF_SRC_NAME		=		$(shell make -C $(LIBPTF_PATH) print-SRCS)
LIBPTF_SRC			=		$(addprefix $(LIBPTF_PATH), $(LIBPTF_SRC_NAME))
LIBPTF_ARCV			=		$(LIBPTF_PATH)libftprintf.a

LIBFT_PATH			=		./$(LIB_DIR)libft/
LIBFT_SRC_NAME		=		$(shell make -C $(LIBFT_PATH) print-SRCS)
LIBFT_SRC			=		$(addprefix $(LIBFT_PATH), $(LIBFT_SRC_NAME))
LIBFT_ARCV			=		$(LIBFT_PATH)libft.a

AR					=		ar rc
RAN					=		ranlib

# ================================ SLAVES ================================= #
MAKESLV				=		make -C


################################################################################
##                                  DELETION                                  ##
################################################################################
RM					=		rm -f
RM_DIR				=		rm -rfd


################################################################################
##                                   DISPLAY                                  ##
################################################################################
# ================================ COLORS ================================= #
RED				=		\033[31m
GRN				=		\033[32m
BLU				=		\033[34m
WHT				=		\033[97m
CYN				=		\033[36m
MGT				=		\033[35m
YLW				=		\033[33m
BCK				=		\033[30m
LBLU			=		\033[94m
PRL				=		\033[38;5;55m
ORG				=		\033[38;5;202m
GRE				=		\033[90m
SLM				=		\033[38;5;203m

# ================================ FORMAT ================================= #
BOLD			=		\033[1m
DIM				=		\033[2m
UNDER			=		\033[4m
BLINK			=		\033[5m
HIDDEN			=		\033[8m
INVERT			=		\033[7m
ITALIC			=		\033[3m
EOF				=		\033[0m

# ============================== CHARACTERS =============================== #
CHECK			=		\\xE2\\x9C\\x94
CROSS			=		\\xE2\\x9C\\x98
HGLSS			=		\\xE2\\xA7\\x97
RAROW			=		\\xE2\\x9E\\x9C
COPYR			=		\\xC2\\xA9

# ================================ MACROS ================================= #
OK				=		$(GRN)$(CHECK)$(EOF)
KO				=		$(RED)$(CROSS)$(EOF)
WAIT			=		$(LBLU)$(HGLSS)$(EOF)
ARROW			=		$(LBLU)$(RAROW)$(EOF)
COFFE			=		\t  S\n\t$(UNDER)C[_]$(EOF)


################################################################################
##                                    RULES                                   ##
################################################################################

all					:		$(NAME_FLOOD_FILL)

proper				:
	@$(MAKESLV) . all
	@$(MAKESLV) . clean



$(OBJS_DIR)			:
	@mkdir -p $@
	@printf "\t$(UNDER)$(BOLD)Created$(EOF) : $@/ $(OK)\n"



$(LIBFT_ARCV)		:		$(LIBFT_SRC)
	@printf "\t$(UNDER)$(BOLD)Creation$(EOF) $@ $(WAIT)\n\t\tPlease, wait..."
	@$(MAKESLV) $(LIBFT_PATH) all
	@printf "\033[K\r"
	@printf "\033[1A"
	@printf "\033[K\r"
	@printf "\t$(UNDER)$(BOLD)Archive created$(EOF) : $@ $(OK)\n"

$(LIBPTF_ARCV)		:		$(LIBPTF_SRC)
	@printf "\t$(UNDER)$(BOLD)Creation$(EOF) $@ $(WAIT)\n\t\tPlease, wait..."
	@$(MAKESLV) $(LIBPTF_PATH) all
	@printf "\033[K\r"
	@printf "\033[1A"
	@printf "\033[K\r"
	@printf "\t$(UNDER)$(BOLD)Archive created$(EOF) : $@ $(OK)\n"



$(OBJS_DIR)%.o		:		$(SRCS_DIR)%.c $(INC_FLOOD_FILL) Makefile
	@printf "\t$(UNDER)$(BOLD)Compiled$(EOF) : $@ $(OK)"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC_COMP)
	@printf "\033[2K\r"

$(NAME_FLOOD_FILL)	:		$(OBJS_DIR) $(LIBFT_ARCV) $(LIBPTF_ARCV) $(OBJS_FLOOD_FILL)
	@$(CC) $(CFLAGS) -o $@ $(OBJS_FLOOD_FILL) $(LIBPTF_ARCV) $(LIBFT_ARCV) $(INC_COMP)
	@printf "\t$(UNDER)$(BOLD)Executable created$(EOF) : $@ $(OK)\n"



clean				:
	@$(RM_DIR) $(OBJS_DIR)
	@printf "\t$(UNDER)$(BOLD)Removed$(EOF) : $(OBJS_DIR) $(OK)\n"
	@$(MAKESLV) $(LIBFT_PATH) clean
	@$(MAKESLV) $(LIBPTF_PATH) clean

fclean				:		clean
	@$(RM) $(NAME_FLOOD_FILL)
	@printf "\t$(UNDER)$(BOLD)Removed$(EOF) : $(NAME_FLOOD_FILL) $(OK)\n"
	@$(MAKESLV) $(LIBFT_PATH) fclean
	@$(MAKESLV) $(LIBPTF_PATH) fclean
	@$(RM_DIR) $(NAME_FLOOD_FILL).dSYM



re					:
	@$(MAKESLV) . fclean
	@$(MAKESLV) . all

norme				:		$(INC_DIR) $(SRCS_DIR)
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@$(MAKESLV) $(LIBFT_PATH) clean
	@$(MAKESLV) $(LIBPTF_PATH) clean

print-%				:
	@echo $($*)

coffee				:
	@echo "Here's a cup of coffee\n$(COFFE)"
	
.PHONY				:		all proper clean fclean re norme coffee
