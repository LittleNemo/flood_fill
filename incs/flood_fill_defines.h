/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill_defines.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 16:08:46 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 16:04:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLOOD_FILL_DEFINES_H
# define FLOOD_FILL_DEFINES_H

# define FF_MSG_USAGE			"usage: ./flood_fill [-p | --print] x y < file"
# define FF_MSG_MALLOC			"Error(flood_fill): malloc failed"
# define FF_MSG_FTYPE			"Error(flood_fill): wrong file type"
# define FF_MSG_FILE			"Error(flood_fill): file not well formated"
# define FF_MSG_COOR			"Error(flood_file): coor not well formated"

# define FF_CHAR_WALL			'#'
# define FF_CHAR_SPACE			'.'
# define FF_CHAR_FILL			'+'

#endif
