/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 15:58:22 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 21:12:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLOOD_FILL_H
# define FLOOD_FILL_H

# include "libft_part_io.h"
# include "libft_part_math.h"
# include "libft_part_string.h"
# include "ft_printf.h"
# include "flood_fill_defines.h"
# include "flood_fill_typedefs.h"

void			ff_error(int code, t_file_h *file_h, t_meta *meta);
void			ff_get_file(t_file_h *file_h);
void			ff_del_file(t_file_h *file_h);
void			ff_get_array(t_file_h *file_h, t_meta *meta);
void			ff_get_start(t_meta *meta, char **av);
void			ff_solve(t_meta *meta, int x, int y);

#endif
