/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill_typedefs.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 16:10:49 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 16:03:31 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLOOD_FILL_TYPEDEFS_H
# define FLOOD_FILL_TYPEDEFS_H

typedef struct	s_file {
	size_t			id;
	char			*line;
	struct s_file_h	*header;
	struct s_file	*next;
	struct s_file	*prev;
}				t_file;

typedef struct	s_file_h {
	size_t			count;
	struct s_file	*head;
	struct s_file	*tail;
}				t_file_h;

typedef struct	s_meta {
	char			**array;
	int				x;
	int				y;
	short			opts;
	short			nb_opts;
}				t_meta;

typedef enum	e_opts {
	FF_OPTS_PRINT
}				t_opts;

typedef enum	e_error {
	FF_NO_ERROR,
	FF_ERROR_USAGE,
	FF_ERROR_MALLOC,
	FF_ERROR_FTYPE,
	FF_ERROR_FILE,
	FF_ERROR_COOR,
	FF_END_ERROR
}				t_error;
#endif
