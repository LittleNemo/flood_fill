/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/23 18:40:26 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 17:48:13 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

void			print_tab(char **area)
{
	int				i;
	int				j;

	i = -1;
	ft_printf("\e[?1049h");
	while (area[++i])
	{
		j = -1;
		while (area[i][++j])
			if (area[i][j] == FF_CHAR_WALL)
				ft_printf("%@ ", BRED, ' ');
			else if (area[i][j] == FF_CHAR_FILL)
				ft_printf("%@ ", BGREEN, ' ');
			else if (area[i][j] == FF_CHAR_SPACE)
				ft_printf("%@ ", BWHITE);
			ft_printf("%@\n", EOC);
	}
	ft_printf("\e[80A");
	usleep(100000);
	ft_printf("\e[?1049l");
}

void			flood_fill(char **tab, t_point size, t_point begin)
{
	if (!tab[begin.x] || !tab[begin.x][begin.y])
		return ;
	if (tab[begin.x][begin.y] == 'F' || tab[begin.x][begin.y] != '.')
		return ;
	tab[begin.x][begin.y] = FF_CHAR_FILL;
	print_tab(tab);
	flood_fill(tab, size, (t_point){begin.x + 1, begin.y});
	flood_fill(tab, size, (t_point){begin.x, begin.y + 1});
	flood_fill(tab, size, (t_point){begin.x - 1, begin.y});
	flood_fill(tab, size, (t_point){begin.x, begin.y - 1});
}

char			**make_area(char **zone)
{
	char			**area;
	int				i;
	int				len;

	len = ft_strlen(*zone);
	if (!(area = (char **)ft_memalloc(sizeof(*area) * (len + 1))))
		return (NULL);
	i = -1;
	while (++i < len)
		if (!(area[i] = ft_strdup(zone[i])))
			return (NULL);
	area[i] = NULL;
	return (area);
}

int				main(void)
{
	char			**area;
	t_point			size = {9, 9};
	t_point			begin = {2, 5};
	char			*zone[] = {
		"#########",
		"#...#...#",
		"#...#...#",
		"#..#....#",
		"###...###",
		"#....#..#",
		"#...#...#",
		"#...#...#",
		"#########",
	};

	area = make_area(zone);
	print_tab(area);
	flood_fill(area, size, begin);
	print_tab(area);
	return (0);
}
