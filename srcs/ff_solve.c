/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ff_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 15:24:51 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 15:36:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

static void		ff_print(char **array)
{
	int				i;
	int				j;

	i = -1;
	ft_printf("\e[?1049h");
	while (array[++i])
	{
		j = -1;
		while (array[i][++j])
			if (array[i][j] == FF_CHAR_WALL)
				ft_printf("%@ ", BRED);
			else if (array[i][j] == FF_CHAR_FILL)
				ft_printf("%@ ", BGREEN);
			else if (array[i][j] == FF_CHAR_SPACE)
				ft_printf("%@ ", BWHITE);
		ft_printf("%@\n", EOC);
	}
	ft_printf("\e[80A");
	usleep(75000);
	ft_printf("\e[?1049l");
}

void			ff_solve(t_meta *meta, int x, int y)
{
	if (!meta->array[x] || !meta->array[x][y])
		return ;
	if (meta->array[x][y] != FF_CHAR_SPACE)
		return ;
	meta->array[x][y] = FF_CHAR_FILL;
	if (meta->opts & (1 << FF_OPTS_PRINT))
		ff_print(meta->array);
	ff_solve(meta, x + 1, y);
	ff_solve(meta, x, y + 1);
	ff_solve(meta, x - 1, y);
	ff_solve(meta, x, y - 1);
}
