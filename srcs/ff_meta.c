/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ff_meta.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 20:50:11 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 15:21:39 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

void			ff_get_array(t_file_h *file_h, t_meta *meta)
{
	size_t			i;
	t_file			*tmp;

	if (!(meta->array = (char **)ft_memalloc(sizeof(meta->array) * \
		(file_h->count + 1))))
		ff_error(FF_ERROR_MALLOC, file_h, NULL);
	i = 0;
	tmp = file_h->head;
	while (tmp)
	{
		if (!(meta->array[i++] = ft_strdup(tmp->line)))
			ff_error(FF_ERROR_MALLOC, file_h, meta);
		tmp = tmp->next;
	}
}

void			ff_get_start(t_meta *meta, char **av)
{
	if (!ft_str_istype(av[0], &ft_issdigit) || \
		!ft_str_istype(av[1], &ft_issdigit))
		ff_error(FF_ERROR_COOR, NULL, meta);
	meta->x = ft_atoi(av[0]);
	meta->y = ft_atoi(av[1]);
}
