/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flood_fill.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 18:12:09 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 16:05:08 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

static void		ff_get_opts(char **av, t_meta *meta)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (av[++i] && av[i][0] == '-')
	{
		j = 0;
		while (av[i][++j])
			if (ft_strequ("-print", av[i] + 1) || av[i][j] == 'p')
				meta->opts |= (1 << FF_OPTS_PRINT);
			else
				ff_error(FF_ERROR_USAGE, NULL, NULL);
		meta->nb_opts++;
	}
}

int				main(int ac, char **av)
{
	static t_file_h	file_h;
	static t_meta	meta;

	ff_get_opts(av, &meta);
	ff_error(!!(ac - meta.nb_opts - 3), NULL, NULL);
	ff_get_start(&meta, av + meta.nb_opts + 1);
	ff_get_file(&file_h);
	ff_get_array(&file_h, &meta);
	ff_del_file(&file_h);
	ft_puttab(meta.array);
	ff_solve(&meta, meta.x, meta.y);
	ft_puttab(meta.array);
	ft_tabdel((void **)meta.array);
	return (0);
}
