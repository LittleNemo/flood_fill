/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ff_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 20:12:49 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/24 21:00:14 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

static void		ff_get_file_update(t_file_h *file_h, char *line)
{
	static size_t	id;
	t_file			*fresh;

	if (!(fresh = (t_file *)ft_memalloc(sizeof(*fresh))))
		ff_error(FF_ERROR_MALLOC, file_h, NULL);
	if (!(fresh->line = ft_strdup(line)))
		ff_error(FF_ERROR_MALLOC, file_h, NULL);
	fresh->id = ++id;
	fresh->header = file_h;
	file_h->count = id;
	if (!file_h->head)
	{
		file_h->head = fresh;
		file_h->tail = fresh;
		return ;
	}
	fresh->prev = file_h->tail;
	file_h->tail->next = fresh;
	file_h->tail = fresh;
}

static void		ff_check_line(t_file_h *file_h, char *line)
{
	while (*line)
	{
		if (*line != FF_CHAR_WALL && *line != FF_CHAR_SPACE)
		{
			ft_strdel(&line);
			ff_error(FF_ERROR_FILE, file_h, NULL);
		}
		line++;
	}
}

void			ff_get_file(t_file_h *file_h)
{
	char			*line;

	line = NULL;
	if (read(0, line, stdin->_file) == -1)
		ff_error(FF_ERROR_FTYPE, NULL, NULL);
	while (get_next_line(stdin->_file, &line))
	{
		ff_check_line(file_h, line);
		ff_get_file_update(file_h, line);
		ft_strdel(&line);
	}
	ft_strdel(&line);
}

void			ff_del_file(t_file_h *file_h)
{
	t_file			*tmp;

	while (file_h->head)
	{
		tmp = file_h->head;
		file_h->head = file_h->head->next;
		ft_strdel(&tmp->line);
		free(tmp);
	}
}
