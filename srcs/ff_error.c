/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ff_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 18:52:22 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/25 14:46:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flood_fill.h"

static char		**ff_get_tab(void)
{
	static char		*tab[FF_END_ERROR] = {
		FF_MSG_USAGE,
		FF_MSG_MALLOC,
		FF_MSG_FTYPE,
		FF_MSG_FILE,
		FF_MSG_COOR,
		NULL,
	};

	return (tab);
}

void			ff_error(int code, t_file_h *file_h, t_meta *meta)
{
	char			**tab;

	if (code == FF_NO_ERROR)
		return ;
	tab = ff_get_tab();
	ft_fprintf(stderr, "%s\n", tab[code - 1]);
	if (meta)
		ft_tabdel((void **)meta->array);
	if (file_h)
		ff_del_file(file_h);
	exit(EXIT_FAILURE);
}
